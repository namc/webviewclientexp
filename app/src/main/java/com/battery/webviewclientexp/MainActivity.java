package com.battery.webviewclientexp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;


public class MainActivity extends Activity {

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.test_text);
        textView.setText("Click me and see a cat!");
        final String url = "http://imgur.com/r/cats/EWUp6v9";

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), WebviewClientActivity.class);
                intent.putExtra(WebviewClientActivity.WEB_URL , url);
                startActivity(intent);
            }
        });
    }
}
